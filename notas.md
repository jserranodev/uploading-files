# "Uploading Files"

## Notas

- Spring MVC selecciona clases anotación `@Controller` conocer rutas.
- Anotaciones `@GetMapping` y `@PostMapping` unen rutas, acciones HTML y métodos del controlador.
- Controlador <-> Servicio <-> datos (sist. archivos, SGBD).
- Fichero `application.properties` ajusta propiedades aplicación.
- Anotación `@SpringBootApplication` equivale a:
  - `@Configuration`
  - `@EnableAutoConfiguration`
  - `@ComponentScan`